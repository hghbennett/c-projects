/************************************************************************************************
Hugh Bennett		9/17/2011		pts:100

Lofton Bullard		COP3530		Program 1


 this assignment you will implement the ADT (abstract data type) character string  using a dynamic array of characters. 
Call the class "My_String". This assignment must be well documented. Following is a description of the assignment. 
If you have any questions you should ask them before the due date of the assignment; otherwise, it will be accepted as correct, 
and that you understand all the instructions.  The C++ library string class should not be used to implement this class.

*************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include "My_String.h"
using namespace std;

/*********************************************************************
 Function Name:  Default Constructor
 Precondition:   Length and Char array have not been initialized.
 Postcondition:	Length and Char array have been initialized.

 Description:  The Default Constructor will initialize your state variables

************************************************************************/
   My_String::My_String( )
  {
	  cout<<"Default constructor has been called!\n";
	 Length = 0;
	 s = new char;


  }

 /*********************************************************************
 Function Name:  My_String (copy constructor)
 Precondition:   
 Postcondition:  deep copy performed

 Description:  Used during a call by value, return, or initialization/declaration

************************************************************************/
   My_String::My_String(const My_String & org)
   {
	   cout<<"the copy constructor was called automatically"<<endl;
	   Length=org.Length;
	   
	   //allocate memory
	 s  = new char[Length];
	for(int i=0; i<Length; i++)
	{
		s[i] = org.s[i];
	}


   }

   /*********************************************************************
 Function Name:  ~My_String (destructor)
 Precondition:   s points to memory allocated using "new".
 Postcondition:  The memory pointed to by s has been de-allocated

 Description:  The destructor will de-allocate all memory allocated for the string

************************************************************************/
  My_String::~My_String()
  {
	  cout<<"The destructor is called automatically\n";
	  delete [] s;//de-allocate dynamic memory pointed to by s.
  }


  /*********************************************************************
 Function Name:  Explicit Value Constructor
 Precondition:   
 Postcondition:  

 Description:  This constructor will have one argument;  a string representing the
 character string to be created.

************************************************************************/
  My_String::My_String(char *c)
  {
	  cout<< "Explicit Value Constructor was called"<<endl; 
	  Length = 0;
	  while(c[Length] != '\0')
	  {
		  Length++;
	  }
	  s=c;
	  s = new char[Length];
	  for(int i=0; i<Length; i++)
	  {
		  s[i] = c[i];
	  }
		
  }

   /*********************************************************************
 Function Name:  Insert 
 Precondition:   substring has not been inserted 
 Postcondition:  Substring was inserted at index i or at the end of string. 

 Description:  Inserts a substring  into string at index i.  
			   If position i does not exist, it inserts the substring at the end of the string

************************************************************************/

  void My_String::insert(My_String & item, int index)
{
	 char* temp = new char[Length + item.Length];//Dynamic String Array;
	 int i, j, k, l;//initalization

	 for ( i = 0; i<index; i++)
	 {
		 temp[i] = s[i]; 
	 }

	 for ( j = 0; j<item.Length; j++,i++)
	 {
		 temp[i] = item.s[j];
	 }

	 for ( k = index + item.Length, l=0; k<(Length + item.Length); k++,l++)
	 {
		 temp[k] = s[l+index];
	 }
	 Length = Length + item.Length;
	 s = temp;
	
 }


  /*********************************************************************
 Function Name:  find
 Precondition:   String B has not been found strarting at a position. 
 Postcondition:  Finds string A in string B starting at position p.

 Description:  Finds string A in string B starting at position p.


************************************************************************/

 int My_String::Find(My_String & A, int start)
 {
	int i,j,index = 0; //initalization

	
	for(i = start ; i<Length ; i++)
	{
		index=start;
		for(j=0;j<A.Length;j++)
		{
			if(s[i] != A.s[j])
			{
				break;//
			}
			i++;
		}
			if(j==A.Length)
			{
				return index;
			}
			else
			{
				i=index;
				start= i+1;
			}
		
	}
	return -1;
 }

		
 


  /*********************************************************************
 Function Name:  Remove
 Precondition:   none
 Postcondition:  item has been removed from the string if it was there.

 Description:  Deletes the first occurrence of a substring from the string if it is there

************************************************************************/

  void My_String::Remove(My_String & item )
  {
	int k;
	int j;
	int prev;
	
	if(IsEmpty())
	{										
		cout<<"EMPTY"<<endl;//if Array is empty
	} 

	else if ((prev = Find(item, 0)) == -1)
	{	
	cout<<" not found."<<endl;//could not find substring 
	
	} 

	else
	{											
	for(k = 0;k < item.Length;k++)
	{
		for(j = prev;j <Length;j++)
		{
			s[j] = s[j + 1];
		}
	}
		Length = Length - item.Length;	//remove substring 			
		
	}
  }
  


  /*********************************************************************
 Function Name:  print
 Precondition:   string is not printed 
 Postcondition:  string is printed 

 Description:  Prints a string

************************************************************************/
void My_String::print()
{
	cout<<"The string is: ";
	for(int i=0; i<Length; i++)// print the strings
	{
	cout<<s[i];
	}
	cout<<endl; 
}

/*********************************************************************
 Function Name:  IsEqual
 Precondition:   Dont know if two strings are equal
 Postcondition:  return true or false if strings are equal or not 

 Description:  Returns true if two strings are equal; otherwise false

************************************************************************/
bool My_String::IsEqual(My_String & A)
{

	 for(int i = 0; i<Length; i++)
	 {
		 if(Length!=A.Length)
		 {
			 return 0;
		 }
		 if (s[i] != A.s[i])//check the cases where the string is not equal 
		 {
			 return 0;
		 }
	 }
	 return 1;
}

/*********************************************************************
 Function Name:  set Equal
 Precondition:   Dont know if two strings are equal
 Postcondition:  return true or false if strings are equal or not 

 Description:  Returns true if two strings are equal; otherwise false

************************************************************************/
char* My_String::SetEqual(My_String & Nstring)//Functon Header
{
	 delete [] s;//delete

	 Length = Nstring.Length;
	 s = new char[Length];
	 for(int i = 0; i<Length; i++)//delete string and set it equal to a new one  4
	 {
		 s[i] = Nstring.s[i]; 
	 }

	 return s;

	 
}


/*********************************************************************
 Function Name:  Findlast  
 Precondition:   none
 Postcondition:  You now know the last accurance of the string 
 Description:  Find the last occurrence of a substring in a string

************************************************************************/
int My_String::FindLast(My_String & item )
{
	int temp = Find(item,0);

	if (temp != -1)
	{ 
		int last = temp;
		while(last !=-1)
		{
			last = Find(item, temp+1);
			if(last != -1)
			{
				cout<<"temp = "<<temp<<endl;
				temp=last;
			}
		}
		return last;
	}
	return -1;
										 
}
	



	


/*********************************************************************F
 Function Name:  AddString
 Precondition:   You have string A and String B
 Postcondition:  String B was Added to the back of String A
 Description:  Adds string B to the back of string A

************************************************************************/
void My_String::AddString(My_String & item)
{
	 char* temp = new char[Length + item.Length]; 
	 int i;
	 int j;

	 for (i = 0; i<Length; i++)
	 {
		 temp[i] = s[i]; 
	 }

	 for (j = 0;j < item.Length; j++)
	 {
		 temp[i] =+ item.s[j]; 
		 i++;
	 }
	 Length = Length + item.Length;
	 delete [] s;
	 s = temp; 
}