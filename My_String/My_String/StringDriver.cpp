/************************************************************************************************
Hugh Bennett		9/17/2011		pts:100

Lofton Bullard		COP3530		Program 1


TIn this assignment you will implement the ADT (abstract data type) character string  using a dynamic array of characters. 
Call the class "My_String". This assignment must be well documented. Following is a description of the assignment. 
If you have any questions you should ask them before the due date of the assignment; otherwise, it will be accepted as correct, 
and that you understand all the instructions.  The C++ library string class should not be used to implement this class.

*************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include "My_String.h"
using namespace std;



int main ()
{
	 
	My_String A("hello_hello_World");//initalize strings 
	My_String B("hello");

	cout<<endl;
	
	cout<<"String Name"<<endl;//Print
	cout<<endl;

	A.print();
	B.print();

	cout<<endl;

	cout<< "Find the last accurance of String B in A : ";//test find last 
	cout<<A.FindLast(B)<<endl;

	cout<<endl;

	cout<<"You will Find String B in A at Position :"<<A.Find(B,0)<<endl;//testing the use of the find function 
	cout<<"You will Find String B in A at Position :"<<A.Find(B,1)<<endl;
	A.FindLast(B);

	cout<<endl;

	cout<<"when B is inserted into A the string is:";//test insert 
	A.insert(B,5);
	A.print();
	
	cout<<endl;

	cout<<"After removing string B from A ";//test remove 
	A.Remove(B);
	A.print();

	cout<<endl;


	cout<<"if A is equal to B then answer is 1 if not amswer is 0:  ";//test if A is equal to B
	cout<< A.IsEqual(B)<<endl;
	cout<<endl;




	cout<<"B is set equal to A  ";//Test setequal function
	B.SetEqual(A);
	B.print();
	
	cout<<endl;

	
	My_String C("Great");//initalize new string
	cout<<endl;
	cout<<"Created String C ";C.print();

	cout<<endl;

	cout<<"A is set equal to C ";//Test setequal function
	A.SetEqual(C);
	A.print();
	
	cout<<endl;

	cout<<"Add String A and String B  ";
	A.AddString(B);//Add A string to the back of the other
	A.print();
	
	cout<<endl;


	

	

	
	
	
	
	
	return 0;
}