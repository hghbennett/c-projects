/************************************************************************************************
Hugh Bennett		9/17/2011		pts:100

Lofton Bullard		COP3530		Program 1


TIn this assignment you will implement the ADT (abstract data type) character string  using a dynamic array of characters. 
Call the class "My_String". This assignment must be well documented. Following is a description of the assignment. 
If you have any questions you should ask them before the due date of the assignment; otherwise, it will be accepted as correct, 
and that you understand all the instructions.  The C++ library string class should not be used to implement this class.

*************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#ifndef My_String_h
#define My_String_h

class My_String 
  {
	 public:
		 bool IsEmpty(){ return Length == 0;}
		 void insert(My_String &,int);
		 void Remove(My_String &);
		 void print();
		 bool IsEqual(My_String &);
		 char* SetEqual(My_String &);
		 int Find(My_String &, int);
		 int FindLast(My_String & );//
		 void AddString(My_String & );//add to end of string 
		 My_String();//default construtor
	     My_String(char*c);//explicit-value constructor to create array of a size specified by the user        
		 My_String (const My_String &);//copy constructor to make deep copy.//purpose is to initialize the state of a class
		~My_String();//destructor
		
		
	private:
		int Length;
		char *s;

  };


  #endif