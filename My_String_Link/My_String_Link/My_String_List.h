/************************************************************************************************
Hugh Bennett		10/6/2011		pts:100

Lofton Bullard		COP3530		Program 2


In this assignment you will implement the ADT (abstract data type) character string  using a singly-linked list of characters. 
Call the class "My_String". 
This assignment must be well documented. 
Following is a description of the assignment. 
If you have any questions you should ask them before the due date of the assignment; 
otherwise, it will be accepted as correct and that you understand all the instructions.

*************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#ifndef My_String_List_h
#define My_String_List_h



class Character
	{
		public:
			char data;	//data item stored in the node
			Character *next; //pointer to next node in list
	};

class My_String_List
  {
	 public:
		 int length();
		 bool IsEmpty(){return front->next==0;};
		 //void insert(My_String_List &,int);
		 void Remove(My_String_List &);
		 void Print();
		 bool IsEqual(My_String_List &);
		 void SetEqual(My_String_List &);
		 Character* Find(My_String_List &);
		 Character* FindLast(My_String_List & );//Check
		 void AddString(My_String_List & );//add to end of string
		 Character * GetBack();
		 void AddChar(char);
		  My_String_List();//default construtor
	      My_String_List(char*c);//explicit-value constructor to create array of a size specified by the user        
		  My_String_List(const  My_String_List &);//copy constructor to make deep copy.//purpose is to initialize the state of a class
		~ My_String_List();//destructor
		
		
	private:
		int Length;
		Character *front;

  };


  #endif