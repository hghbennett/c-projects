/************************************************************************************************
Hugh Bennett		10/6/2011		pts:100

Lofton Bullard		COP3530		Program 2


In this assignment you will implement the ADT (abstract data type) character string  using a singly-linked list of characters. 
Call the class "My_String". 
This assignment must be well documented. 
Following is a description of the assignment. 
If you have any questions you should ask them before the due date of the assignment; 
otherwise, it will be accepted as correct and that you understand all the instructions.

*************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include "My_String_List.h"
using namespace std;

/*********************************************************************
 Function Name:  Default Constructor
 Precondition:   Length and Char array have not been initialized.
 Postcondition:	Length and Char array have been initialized.

 Description:  The Default Constructor will initialize your state variables

************************************************************************/
   My_String_List::My_String_List( )
  {
	  cout<<"Default constructor has been called!\n";
	 front = new Character;
	 front->next = NULL;


  }

   /*********************************************************************
 Function Name:  My_String (copy constructor)
 Precondition:   
 Postcondition:  deep copy performed

 Description:  Used during a call by value, return, or initialization/declaration

************************************************************************/
   My_String_List::My_String_List(const My_String_List & org)
   {
	   cout<<"the copy constructor was called automatically"<<endl;
	   Character *p = (org.front)->next;
	   front = new Character;
	while(p!=NULL)
	{
		AddChar(p->data);
		p=p->next;
	}
   }

   /*********************************************************************
 Function Name:  ~My_String (destructor)
 Precondition:   s points to memory allocated using "new".
 Postcondition:  The memory pointed to by s has been de-allocated

 Description:  The destructor will de-allocate all memory allocated for the string

************************************************************************/
  My_String_List::~My_String_List()
  {
	  cout<<"The destructor is called automatically\n";
	  Character *p;


		while (front->next != NULL)
		{
			p = front->next;
			front->next = p->next;
			delete p;

		}
		delete front;
  }

  /*********************************************************************
 Function Name:  Explicit Value Constructor
 Precondition:   
 Postcondition:  

 Description:  This constructor will have one argument;  a string representing the
 character string to be created.

************************************************************************/
  My_String_List::My_String_List(char *c)
  {
	  cout<< "Explicit Value Constructor was called"<<endl; 
	  front = new Character;
	  front->next= NULL;
	  Length = 0;
	  while(c[Length] != NULL )
	  {
		 
		  AddChar (c[Length++]);
	  }
	 
  }



  void My_String_List::AddChar(char x)
  {
	  Character * ptr = new Character;
	  Character * back = GetBack();
	  ptr->next = NULL;
	  ptr->data = x;
	  if(back==NULL)
	  {
		  front = ptr;
	  }
	  else
	  {
		  back->next = ptr;
	  }


  }

  Character * My_String_List::GetBack()
  {
	  Character *p = front;
	  while (p != 0 && p->next != NULL)
	  {
		  p = p->next;
	  }
	  return p;
  }



  int My_String_List::length()
  {
	  Character *p = front->next;
	  int len = 0;
	  
	  while(p != NULL)
	  {
		  p=p->next;
		  len++;
	  }
	  return len;
	  
  }


 /*********************************************************************
 Function Name:  Print
 Precondition:   
 Postcondition: Print String

 Description: Prnts a string 

************************************************************************/
   void My_String_List::Print()
   {
	   Character *temp = front->next;
	   if(temp == NULL)
	   {
		cout<<"Sorry this list is empty"<<endl;
	   }
	   else
	   {
		 while(temp != NULL)
		 {
			cout<<temp->data;
			temp = temp -> next;
		 }
		
		cout<<endl;
	   }

   }

   /*********************************************************************
 Function Name:  find
 Precondition:   String B has not been found strarting at a position. 
 Postcondition:  Finds string A in string B starting at position p.

 Description:  Finds string A in string B.


************************************************************************/

Character *My_String_List::Find(My_String_List & org)
 {
	 Character *Start = front->next;
	 Character *A    = front->next;
	 Character *B = org.front->next;

	 while( Start != NULL)
	 {
		 while (A != NULL && B != NULL)
		 {
		 
			if( A->data != B->data)
			{
				break;
			}
			A=A->next;
			B=B->next;
		 }
	 
		 

		if(B==NULL)
		{
			return Start;
		}
	 
		Start = Start->next;
		A= Start;
		B= org.front;

	 }

	 
	 return NULL;
 }


/*********************************************************************
 Function Name:  Findlast  
 Precondition:   none
 Postcondition:  You now know the last accurance of the string 
 Description:  Find the last occurrence of a substring in a string

************************************************************************/
Character *My_String_List::FindLast(My_String_List & item )
{
	
		Character *temp = Find(item);//initialization 
		
		cout<<"temp: "<< temp<<endl;
		if(temp != NULL)
		{
			Character *last = temp->next;
			Character *B = item.front->next;

			while(last != NULL && B != NULL)
			{
				
				 
		 
				if( last->data != B->data)
				{
					
					break;
				
				}
				last->next;
				B=B->next;
		
			}
			cout<<"last: "<< last<<endl;
			return last;
			
		}

		return NULL;	
}
	   
/*********************************************************************F
 Function Name:  AddString
 Precondition:   You have string A and String B
 Postcondition:  String B was Added to the back of String A
 Description:  Adds string B to the back of string A

************************************************************************/
void My_String_List::AddString(My_String_List & item)
{
	 Character *temp = GetBack(); 
	 Character *additem = item.front->next;

	 if (!item.IsEmpty())
	 {
		while(additem != NULL)
		{
			AddChar(additem->data);
			additem=additem->next;
		}
	 }

	
}

/*********************************************************************
 Function Name:  IsEqual
 Precondition:   Dont know if two strings are equal
 Postcondition:  return true or false if strings are equal or not 

 Description:  Returns true if two strings are equal; otherwise false

************************************************************************/
bool My_String_List::IsEqual(My_String_List & B)
{
	Character* A_List = front->next;
	Character* B_List = B.front->next;
	bool i;

	 while(A_List != NULL && B_List != NULL)
	 {
		 if(Length!=B.Length)
		 {
			 i=0;
			 return i;
		 }
		 else if (A_List->data == B_List->data)//check the cases where the string is not equal 
		{
			A_List=A_List->next;
			B_List=B_List->next;
			i=1;
		 }
		 else{return 0;}
	 }
	 return i;
}

/*********************************************************************
 Function Name:  set Equal
 Precondition:   Dont know if two strings are equal
 Postcondition:  return true or false if strings are equal or not 

 Description:  Set two strings equal to each other 

************************************************************************/
void My_String_List::SetEqual(My_String_List & Nstring)//Functon Header
{
	Character* A_List;//so value won't change
	Character* B_List = Nstring.front->next;//so value won't change
	Character* temp = new Character;
	cout<<"set equal was called"<<endl;
	if(IsEmpty())
	{
		front = new Character;
		A_List = front->next;	
	}
	else 
	{
		 A_List = B_List;
	}
			
	while(B_List!= NULL)
	{
		temp->data = B_List->data;
		temp->next = new Character;
		temp = temp->next;
		B_List = B_List->next;
	}
	
	temp->next = NULL;

	

	 
	 
}


 
void My_String_List::Remove(My_String_List & item)
{
	cout<<"Remove as been called"<<endl;
	Character *p= Find(item);
	if(IsEmpty())
	{
		cout<<"Cant delete item or list is empty"<<endl;
	}
	else
	{
		Character *back = front->next;
		if (p==front->next && front->next->next==0)
		{
			delete front->next;
			front->next=0;
		}
		else
		{
			if(back != p)
			{
				while(back->next !=p)
				{
					back = back->next;
				}
				back->next = p->next;
				delete p;
			}
			else
			{
				front->next= front->next;
				delete p; 
			}
	
			

			
		}
	}



}