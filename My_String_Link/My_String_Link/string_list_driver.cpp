/************************************************************************************************
Hugh Bennett		10/6/2011		pts:100

Lofton Bullard		COP3530		Program 2


In this assignment you will implement the ADT (abstract data type) character string  using a singly-linked list of characters. 
Call the class "My_String". 
This assignment must be well documented. 
Following is a description of the assignment. 
If you have any questions you should ask them before the due date of the assignment; 
otherwise, it will be accepted as correct and that you understand all the instructions.

*************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include "My_String_List.h"
using namespace std;



int main ()
{

	My_String_List List_C; 
	My_String_List List_A("hello_Day");//initalize strings 
	My_String_List List_B("hello");
	
	cout<<endl;
	
	cout<<"String Name"<<endl;//Print
	cout<<endl;
	
	
	cout<<List_B.length()<<endl;
	cout<<List_A.length()<<endl;
	List_A.Print();
	List_B.Print();
	cout<<List_A.Find(List_B)<<endl;
	
	List_A.FindLast(List_B);
	cout<<endl;
	List_A.AddString(List_B);
	List_A.Print();
	List_A.SetEqual(List_B);
	List_A.Print();

	List_A.Remove(List_B);
	List_A.Print();


cout<< "If 1 list are equal,if 0 list are not Equal:"<< List_A.IsEqual(List_B)<<endl;


	return 0;
}