/*-----------------------------------------------------------------------------------------------
Hugh Bennett		11/8/2011		pts:100

Lofton Bullard		COP3530		Program 4


 After completing this assignment you will be able to do the following:

(1) manipulate pointers, (2) allocate memory dynamically, 
(3) implement a default constructor, copy constructor and destructor, 
(4) use only one pointer to delete from the back and to add to the front of the queue.

--------------------------------------------------------------------------------------------------*/

#include<iostream>
#include<string>
#include "CQUEUE.h"

using namespace std;



int main()
{
	CQUEUE test;
	CQUEUE test2;
	
	test.enqueue("Suzzan");//add to queue from the back.
	test.enqueue("I");//add to queue from the back.
	test.enqueue("Love");//add to queue from the back.
	test.enqueue("You");//add to queue from the back.
	test.print();//Display to screen.
	test.dequeue();//delete from the front.
	test.enqueue("Lewis");

	test.print();
	
	
	

	
	CQUEUE A = test;
	A.print();

	
	return 0;
	
}