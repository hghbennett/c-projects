/*-----------------------------------------------------------------------------------------------
Hugh Bennett		11/8/2011		pts:100

Lofton Bullard		COP3530		Program 4


 After completing this assignment you will be able to do the following:

(1) manipulate pointers, (2) allocate memory dynamically, 
(3) implement a default constructor, copy constructor and destructor, 
(4) use only one pointer to delete from the back and to add to the front of the queue.

--------------------------------------------------------------------------------------------------*/

#include <iostream>
#include <string>


using namespace std;

#ifndef CQUEUE_h
#define CQUEUE_h

class qnode
    {
        public:
            string name;
            qnode *prev, *next;
    };

    class CQUEUE
    {
        public:
            CQUEUE( );//Constructor.
           ~CQUEUE( );//Destructor.
           CQUEUE(const CQUEUE &);//copy constructor.
           void enqueue(string);//add a string to queue.
           void dequeue( );//delete a string to queue.
           void print( );//Display.
		   bool is_Empty(){return front == NULL;};//find out if queue is empty.
       private:
           qnode *front;
    };

#endif