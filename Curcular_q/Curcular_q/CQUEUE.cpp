/*-----------------------------------------------------------------------------------------------
Hugh Bennett		11/8/2011		pts:100

Lofton Bullard		COP3530		Program 4


 After completing this assignment you will be able to do the following:

(1) manipulate pointers, (2) allocate memory dynamically, 
(3) implement a default constructor, copy constructor and destructor, 
(4) use only one pointer to delete from the back and to add to the front of the queue.

--------------------------------------------------------------------------------------------------*/

#include<iostream>
#include<string>
#include "CQUEUE.h"

using namespace std;
/*--------------------------------------------------------------------------------------------------------------------------------
Function name: Constructor

Precondition: None.
Postcondition: An empty Queue object has been constructed.
(front are initialized to NULL pointers).

 Description: The Default Constructor will initialize your state variables.
-----------------------------------------------------------------------------------------------------------------------------------*/
CQUEUE::CQUEUE()
{
	cout<<"Default constructor automatically called"<<endl;
	front = NULL;//list is empty.
}

/*--------------------------------------------------------------------------------------------------------------------------------
Function name: Copy Constructor

Precondition: original is the queue to be copied and is received
as a const reference parameter.
Postcondition: A copy of original has been constructed.

 Description: The copy Constructor will copy the original.
-----------------------------------------------------------------------------------------------------------------------------------*/
CQUEUE::CQUEUE(const CQUEUE & org)
{
	front = NULL;
	qnode *p = org.front;
	cout<<"Copy Constructor was called"<<endl;
	if(org.front == 0)
	{
		cout<<"No list to copy"<<endl;
		front=0;
	    
	}
	
		
		//front = new qnode;
		//front->name = p->name;
		//p = p->next;
		else if (p->next == p)
		{
			enqueue(p->name);
		}
		else
		{
			while(p->next!=org.front )
			{
				enqueue(p->name);
				p=p->next;
				
			}
		}

    cout<<"got to end"<<endl;
}

/*--------------------------------------------------------------------------------------------------------------------------------
Function name: Destructor

Precondition: none.
Postcondition: The linked list in the queue has been deallocated.

 Description: deallocate the list in the queue.
-----------------------------------------------------------------------------------------------------------------------------------*/
CQUEUE::~CQUEUE()
{
	cout<<"Destructor was automatically called"<<endl;
	/*if(front != NULL)
	{
		qnode *p = front;
		while(p != front->prev)
		{
			front = front->next;
			delete p;
		}
	}
	delete front;*/
	while(front != NULL)
	{
		dequeue();

	}

}

/*--------------------------------------------------------------------------------------------------------------------------------
Function name: Enqueue

Precondition: value is to be added to this queue.
Postcondition: value is added at back of queue.

 Description: addeds a qnode with a string.
-----------------------------------------------------------------------------------------------------------------------------------*/
void CQUEUE::enqueue(string data)
{
	cout<<"Enqueue was called"<<endl;
	if(is_Empty())//if list is empty.
	{
		front = new qnode;
		front->name = data;
		front->next = front;
		front->prev = front;
	}
	else//if list has multiple qnode
	{
		qnode *p = new qnode;
		p->name = data;
		front->prev->next = p;
		p->next = front;
		p->prev = front->prev;
		front->prev = p;
	}
}

/*--------------------------------------------------------------------------------------------------------------------------------
Function name: Print

Precondition: list need to be displayed to screen.
Postcondition: Queue's contents, from front to back, have been output to display.

 Description: Display string in the queue.
-----------------------------------------------------------------------------------------------------------------------------------*/
void CQUEUE::print()
{
	if(is_Empty())
	{
		cout<<"System Error Queue is Empty !!!!"<<endl;
	}
	else
	{
		qnode *p = front;
		while(p != front->prev)
		{
			cout<<p->name<<" ";
			p = p->next;
		}

		cout<<p->name<<endl;
	}

}

/*--------------------------------------------------------------------------------------------------------------------------------
Function name: dequeue

Precondition: Queue is either empty,nonempty or multiple qnodes.
Postcondition: string at front of queue has been  removed, unless queue is empty, in case, error message is displayed and execution allowed 
to proceed.

 Description: Remove string at front of queue(if any).
-----------------------------------------------------------------------------------------------------------------------------------*/
void CQUEUE::dequeue()
{
	cout<<"Dequeue was called"<<endl;
	if(is_Empty())//if list is empty 
	{
		cout<<"System Error cannot Dequeue. Queue is empty!!!!"<<endl;
	}
	else if (front == front->prev)//if list is equal to front 
	{
		
		front=NULL;
		

	}
	else//if mulitiple qnodes
	{
		
		qnode *p = front;
		front = front->next;
		front->prev = p->prev;
		p->prev->next = front;
		delete p;
		
		
	}



}