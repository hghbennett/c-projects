#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
using namespace std;



const int SIZE = 22;
  class call_record
  {
  public:
	string cell_phone_number;
	int number_of_relays;
	int minutes;
	double net_cost;
	double tax_rate;
	double call_tax;
	double total_cost_of_call;

  };

  void initialize_DB(call_record [], int &);
  
  int* search(call_record [],int,string);
  void process_data_DB(call_record[],int,double,double,double,double);
  void call_stats(call_record [], int ,string);
  void print(call_record [],int);
  void add(call_record[], int &);
  void remove(call_record[], int &);
  void file_print(call_record[],int);
  bool Is_Full(int count){ return count == SIZE;}
  bool Is_Empty(int count){ return count == 0;}


  void initialize_DB(call_record call_DB[], int & count)
  {
	  ifstream in;
	
	 

	in.open("call_data.txt");

	while (!in.eof())
	{
		in>>call_DB[count].cell_phone_number;
		if(!in.eof())
		{
				in>>call_DB[count].number_of_relays;
				

				in>>call_DB[count++].minutes;

				
		}

	}

	in.close();

  }

 /*********************************************************************
 Function Name:  search
 Precondition:   call_DB and count have been intialized.
 Postcondition:  cell_phone_number was found/not found in call_DB;
                 the location is returned (-1 if not in call_DB).

 Description:  Tbe function searches for cell_phone_number in call_DB;
               if cell_phone_number is in call_DB its location is returned;
			   otherwise -1;
************************************************************************/
int* search(call_record call_DB[], int count, string cell_no)
{
	int* pointer;
	int list[SIZE];
	int j = 0;
	cout<<endl;
	cout<<"Enter the cell phone number of the customer you are searching for:";
	cin>>cell_no;
	for(int i=0; i<count; i++)
	{
		if (cell_no == call_DB[i].cell_phone_number)
			{
				list[j] = i;
				j++;
			}
		list[j] = -1;
	}
	pointer=list;
		return pointer;
	
}


 void process_data_DB(call_record call_DB[],int count)
 {
	 for(int i=0;i<count;i++)
	 {
		 if(0<= call_DB[i].number_of_relays && call_DB[i].number_of_relays   <=5){call_DB[i].tax_rate=0.01;}
		 else if(6<= call_DB[i].number_of_relays && call_DB[i].number_of_relays  <=11){call_DB[i].tax_rate=0.03;}
		 else if(12<= call_DB[i].number_of_relays && call_DB[i].number_of_relays <=20){call_DB[i].tax_rate=0.05;}
		 else if(21<= call_DB[i].number_of_relays && call_DB[i].number_of_relays <=50){call_DB[i].tax_rate=0.08;}
		 else if( call_DB[i].number_of_relays && call_DB[i].number_of_relays >50){call_DB[i].tax_rate=0.12;}
	 
	

		call_DB[i].net_cost=double(call_DB[i].number_of_relays) / 50 * 0.40 *call_DB[i].minutes;//calculations for netcost.
		call_DB[i].call_tax=call_DB[i].net_cost*(call_DB[i].tax_rate);//calculate tax on call.
		call_DB[i].total_cost_of_call=call_DB[i].net_cost+call_DB[i].call_tax;//calculation.
	 }

	
	 

 }

  void call_stats(call_record call_DB[], int count,string cell_no)
  {
	  
	  int* pointer=search(call_DB,count,cell_no);
	  int key[SIZE];
	  for(int i = 0;i<SIZE;i++)
	  {
		  key[i]=pointer[i];
		  if(key[i]==-1)
			  break;
	  }
	
	  if (key[0]==-1){cout<<"this number is not in the system"<<endl;}
	  
	  else
	  {
		  int i = 0;
		  cout<<call_DB[key[i]].cell_phone_number<<"   \t";
		  for(i = 0;i<SIZE;i++)
		  {
			  if(key[i]==-1)
				break;
			cout<<call_DB[key[i]].number_of_relays;
			cout<<setw (10)<<call_DB[key[i]].minutes;
			cout<<setw (10)<<call_DB[key[i]].net_cost;
			cout<<setw (10)<<call_DB[key[i]].tax_rate;
			cout<<setw (10)<<setprecision (3)<<call_DB[key[i]].call_tax;
			cout<<setw (10)<<setprecision (4)<<call_DB[key[i]].total_cost_of_call<<endl<<endl;
			cout<< setw(17);
			
		  }

	  }
	  


	  
  }


/*********************************************************************
 Function Name:  add
 Precondition:   call_DB and count have been intialized.
 Postcondition:  Record has been added to call_DB and call_DB maybe full, or
                 record has not been added because call_DB is full, or

 Description:  Tbe function adds a record to call_DB.

************************************************************************/
			
  void add(call_record call_DB[], int & count)
  {
	  char yes_or_no;
	 cout<<" would you like to add information to the database enter (y/n)?"<<endl;
	 cin>>yes_or_no;
	 if (yes_or_no=='y')
	 {

	  if (!Is_Full(count))
	  {
		  
		  cout<<"Enter cell phone number: ";
		  cin>>call_DB[count].cell_phone_number;
		  cout<<"Enter number of relays: ";
		  cin>>call_DB[count].number_of_relays;
		  cout<<"Enter minutes used: ";
		  cin>>call_DB[count].minutes;
		  count++;
		  cout<<"this data was added to the database"<<endl;
	  }
	  else
	  {
		  cout<<"The array is full\n";
	  }
	 }
  }


  void remove(call_record call_DB[], int &count)
 {
	 string cell_number;
	 int j;
	 char yes_or_no;
	 cout<<" would you like to delete information to the database enter (y/n)?"<<endl;
	 cin>>yes_or_no;
	 if (yes_or_no=='y')
	 {
	 if (!Is_Empty(count))
	 {
		
		 int* i = search(call_DB,count,cell_number);

		 if(i[0]!=-1)
		 {
			for(j=i[0]; j<count-1;j++)
			{
			 call_DB[j] = call_DB[j+1];
			}
			count--;
			cout<<"this data was removed from the database"<<endl;
		 }
	 }
	 else
	 {
		 cout<<"The array is empty\n";
	 }
	 }
 }




  void print(call_record call_DB[], int count)
  {
	  for(int i=0; i<count; i++)
		cout<<call_DB[i].cell_phone_number<<"   \t"
			<<call_DB[i].number_of_relays<<"   \t"
			<<call_DB[i].minutes<<"   \t"
	        <<call_DB[i].tax_rate<<endl;

  }

  void file_print(call_record call_DB[], int count)
  {
	  ofstream out;  //declaring an output file stream
	  string filename;
	  cout<<"Enter output filename: ";

	  cin>>filename;
	  
	  out.open(filename.c_str());

	  for(int i=0; i<count; i++)
		out<<call_DB[i].cell_phone_number<<'\t'
			<<call_DB[i].number_of_relays<<'\t'
			<<call_DB[i].minutes<<'\t'
			<<setprecision (2)<<fixed<<call_DB[i].net_cost<<'\t'
			<<call_DB[i].tax_rate<<'\t'
			<<setprecision (2)<<fixed<<call_DB[i].call_tax<<'\t'
			<<setprecision (2)<<fixed<<call_DB[i].total_cost_of_call<<endl;

  }




int main()
{
	int count = 0; //tells how many records;next available location
	call_record call_DB[SIZE];
	string cell_no;


	initialize_DB( call_DB,count);
	process_data_DB( call_DB, count);
	call_stats( call_DB, count, cell_no);
	add(call_DB,count);
	remove( call_DB,count);
	file_print(call_DB,count);
	//print(call_DB,count);
	

	

	return 0;
}