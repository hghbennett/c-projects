///************************************************************************************************
//Hugh Bennett		9/17/2011		pts:100
//
//Lofton Bullard		COP3530		Program 5
//
//
// The purpose of this program is to give you experience implementing a hash table, handling collisions, 
//and using hashing with a linear function. Hashing is a method that enables access to table items (adding, deleting, seaching and so forth) in time that is relatively constant and independent of the items in the table.
//Hashing uses a hash function and a scheme for resolving collisions. A hash function is a function that maps the search key of a table item into a location that will contain that item.
//A collision occurs when a hash function maps two or more distinct search keys into the same location.
//*************************************************************************************************/
#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

class Hash_DB
{
public:
   string info;
   Hash_DB *lchild , *rchild;
};

class Clients_Address_Book
{
public:
Clients_Address_Book();
void insert(Hash_DB * &, string item);
void del(Hash_DB * &, string item);
Hash_DB * search_tree(Hash_DB *, string item);
Hash_DB * inorder_succ(Hash_DB *);
Hash_DB * parent(Hash_DB *);
void delete_tree(Hash_DB * &);
void print_tree(Hash_DB *);
bool empty(Hash_DB *root ){ return (root==0);}

private:
	Hash_DB * root;

};

     
Clients_Address_Book::Clients_Address_Book()
{
	root = NULL;


}
void Clients_Address_Book::insert(Hash_DB * & loc_ptr, string item)
{
   ifstream in_stream;
	in_stream.open("tree.txt");
	if(in_stream.fail())
	{
		cout << "No File was Found";
	}
	else
	{
		string newline;
		Hash_DB* pNewNode;
		int count = 0;
		do
		{
			if(root==NULL)
			{
				getline(in_stream,newline);
				pNewNode = new Hash_DB;
				pNewNode->info = newline;
				count++;
				root = pNewNode;
				root->lchild = NULL;
			}
			else
			{
				getline(in_stream,newline);
				pNewNode = new Hash_DB;
				pNewNode->info = newline;
				 count++;
				pNewNode->lchild = root;
				root = pNewNode;
			}
		}while(!in_stream.eof());
	}
}

Hash_DB * Clients_Address_Book::search_tree(Hash_DB * loc_ptr, string item)
{
    if (loc_ptr!=0)
    {
       if(loc_ptr->info==item)
          return loc_ptr;
       else if (loc_ptr->info>item)
          return search_tree(loc_ptr->lchild,item);
       else
          return search_tree(loc_ptr->rchild,item);
    }
    else
         return loc_ptr;
}

void Clients_Address_Book:: del(Hash_DB * & loc_ptr, string item)
{

   if (loc_ptr==0)
       cout<<"item not in tree\n";

   else if (loc_ptr->info > item)
       del(loc_ptr->lchild, item);

   else if (loc_ptr->info < item)
       del(loc_ptr->rchild, item);
   
   else
   {
       Hash_DB * ptr;

       if (loc_ptr->lchild == 0)
       {
          ptr=loc_ptr->rchild;
          delete loc_ptr;
          loc_ptr=ptr;
       }
       else if (loc_ptr->rchild == 0)
       {
          ptr=loc_ptr->lchild;
          delete loc_ptr;
          loc_ptr=ptr;
       }
       else
       {
          ptr=inorder_succ(loc_ptr);
          loc_ptr->info = ptr->info;
          del(loc_ptr->rchild, ptr->info);
       }
    }

}


Hash_DB *Clients_Address_Book::inorder_succ(Hash_DB * loc_ptr)
{

  Hash_DB *ptr=loc_ptr->rchild;

  while(ptr->lchild!=0)
  {
     ptr=ptr->lchild;
  }
  return ptr;
}
    
void Clients_Address_Book::print_tree(Hash_DB * root)
{
  if (root!=0)
  {
   print_tree(root->lchild);
   cout<<root->info<<endl;
   print_tree(root->rchild);
  }
}


void Clients_Address_Book:: delete_tree(Hash_DB * & root)
{
   if (root!=0)
   {
     delete_tree(root->lchild);
     delete_tree(root->rchild);
     delete root;
   }
}


int main()
{
    Hash_DB *root=0;
	Hash_DB *root1=0, *root2=0;
    string s;
    char ch;
    string key3; 
    string key4;
	string response;
	Clients_Address_Book k;
    cout<<"enter command, i,s,d,p,e:";
    cin>>ch;
    cout<<endl;

    while (ch!='e')
    {
       switch (ch)
       {
	   case 'i'  :cout<<"enter string: ";
             cin>>s;
             k.insert(root,s);
             break;
 
       case 's'  :cout<<"enter word to search for: ";
             cin>>s;
             if (!k.search_tree(root,s))
                cout<<"word not in tree\n";
             else
                cout<<s<<" was found in the tree\n";
             break;
       case 'd'  :cout<<"enter word to delete: ";
                  cin>>s;
                  k.del(root,s);
                  break;
       case 'p'  :cout<<"...printing tree...\n";
             k.print_tree(root);
			 break;
	   
				default:break;
      }
      cout<<"enter command, i,s,d,p,e: ";
      cin>>ch;
      cout<<endl;
    }
    k.delete_tree(root);
    cout<<endl<<"no more binary tree.....\n";
    return 0;
} 
     


